<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Public/Css/estilos.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FARMACIA VENTAS</title>
</head>
<body>
<section id='contenido' class='col-xs-12 col-sm-12 col-md-12'>

    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 2%">
        <div class="container-fluid">
            <a class="navbar-brand" href="simi.ico"><h2>FARMACIA</h2></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=Registrarbd">
                            REGISTRO DE PRODUCTOS
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=Editar">
                            EDICION DE PRODUCTOS
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=producto">
                            DESCRIPCION DE PRODUCTOS
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=vistaCambioCantidad">
                           VENTA DE PRODUCTOS
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div>
        <?php
        use Modelos\productos;
        $productos = Producto::consultar();
        $fecha = Producto::fecha();

        if(isset($_GET['productos'])){
            echo"<center><h2>Producto no seleccionado</h2></center>";
        }
        echo"<form method='POST' action='../repositorio/?controller=user&action=Cantidad'> 
                    <div class='dropdown'>
                        <select name='id' class='center' required>
                            <option value='0'>Seleccione producto</option>";
        while ($valores = mysqli_fetch_array($productos)) {
            echo "<option value=$valores[id]>$valores[id].-$valores[nombre]</option>";
        }
        echo"</select>
                    </div>
                    <button type='submit' class='btn btn-primary center'>Buscar</button>
                </form>";

        if(isset($_POST['id'])){
            if($_POST['id'] > 0){

                $producto = Producto::consultar($_POST['id']);

                //Formulario pre-llenado con los datos de la consulta del producto
                echo"<form method='POST' action='../repositorio/?controller=user&action=Cantidad'>
                            <div class='from-grup center'>
                                <label>id</label>
                                <input type='hidden' class='form-control' name='id' value='$producto->id'>
                                <input type='text' class='form-control' name='' value='$producto->id' disabled>
                                <label>NOMBRE PRODUCTO</label>
                                <input type='hidden' class='form-control' name='nombre' value='$producto->id'>
                                <input type='text' class='form-control' name='' value='$producto->Nombre' disabled>
                            </div>
                            <div class='from-grup center'>
                                <label>CANTIDAD </label>
                                <input type='number' min='0' class='form-control' name='cantidad' placeholder='Cantidad de cambio' required>
                            </div>
                            <div class='form-check-inline center'>
                                <input class='form-check-input' type='radio' name='sentido' value='resta' required>
                                <label class='form-check-label' for='resta'>Restar</label>
                                <input class='form-check-input' type='radio' name='sentido' value='suma'>
                                <label class='form-check-label' for='suma'>Sumar</label>
                            </div>
                            <div>
                                <button type='submit' class='btn btn-primary center'>Enviar</button>
                            </div>
                        </form>";
                if(($producto->AnioCaducidad == $fecha['anio'] && $producto->MesCaducidad == $fecha['mes'] && ((($producto->DiaCaducidad)-($fecha['dia'])) < 5)) || ($producto->MesCaducidad < $fecha['mes'] && $producto->DiaCaducidad == $fecha['anio']) || $producto->AnioCaducidad < $fecha['dia']){
                    echo"<div class='from-grup'>
                                <small id='caducidad' class='form-text aviso'>El producto caduca en menos de 5 dias.</small>
                            </div>";
                }
                if($producto->cantidad < 5){
                    echo"<div class='from-grup'>
                                <small id='PRODUCTOS' class='form-text aviso'>El producto tiene menos de 5 productos.</small>
                            </div>";
                }
            }
            else{
                header("location:../repositorio/?controller=user&action=vistaCambioCantidad&failure");
            }
        }
        ?>
    </div>
</section>
</body>
</html>
