<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Public/Css/estilos.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IFORMACION PRODUCTOS</title>
</head>
<body>
<section id='contenido' class='col-xs-12 col-sm-12 col-md-12'>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 ">
        <div class="container-fluid">
            <a class="navbar-brand" href=""><h2>FARMACIA</h2></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href=" /repositorio/?controller=User&action=registrarbd">
                            Registro de productos
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=editarbd">
                            Edicion de productos
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=producto">
                           Descripcion de productos
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=vistaCambioCantidad">
                            Venta de productos
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?php
    use Modelos\Productos;
    $productos = Producto::consultarTodo();
    $fecha = Producto::fecha();
    if(isset($_GET['productos'])){
        echo"<center><h2>Producto no seleccionado</h2></center>";
    }
    echo"<form method='POST' action='../repositorio/?controller=user&action=producto'> 
                <div class='dropdown'>
                    <select name='id' class='center' required>
                        <option value='0'>Seleccione producto</option>";
    while ($valores = mysqli_fetch_array($productos)) {
        echo "<option value=$valores[id]>$valores[id].-$valores[nombre]</option>";
    }
    echo"</select>
                </div>
                <button type='submit' class='btn btn-primary center'>Buscar</button>
            </form>";

    if(isset($_GET['id'])){
        if($_GET['id'] > 0){
            $producto = Producto::consultar($_GET['id']);
            echo"<div>
                        <div class='from-grup'>
                            <label>Nombre</label>
                            <input type='text' class='form-control' name='Nombre-producto' placeholder='' value='$producto->nombre' disabled>
                        </div>
                        <div class='from-grup'>
                            <label>Caducidad</label>
                            <input type='text' class='form-control' name='dia-caducidad' placeholder='Dia' value='$producto->DiaCaducidad' disabled>
                            <input type='text' class='form-control' name='mes-caducidad' placeholder='Mes' value='$producto->MesCaducidad' disabled>
                            <input type='text' class='form-control' name='año-caducidad' placeholder='Año' value='$producto->AnioCaducidad' disabled>
                        </div>
                        <div class='from-grup'>
                            <label>Cantidad</label>
                            <input type='text' class='form-control' name='cantidad' placeholder='' value='$producto->Cantidad' disabled>
                        </div>
                        <div class='from-grup'>
                            <label>Costo</label>
                            <input type='text' class='form-control' name='costo' placeholder='' value='$producto->Costo' disabled>
                        </div>
                    </div>";
            if(($producto->DiaCaducidad == $fecha['dia'] && $producto->MesCaducidad == $fecha['mes'] && ((($producto->AnioCaducidad)-($fecha['anio'])) < 5)) || ($producto->caducidadM < $fecha['mon'] && $producto->caducidadA == $fecha['year']) || $producto->caducidadA < $fecha['year']){
                echo"<div class='from-grup'>
                            <small id='anuncio-caducidad' class='form-text aviso'>El producto tiene menos de 5 dias por caducar .</small>
                        </div>";
            }
            if($producto->Cantidad < 5){
                echo"<div class='from-grup'>
                            <small id='anuncio-existencia' class='form-text aviso'>Solo quedan menos de 5 productos de estos.</small>
                        </div>";
            }
        }
        else{
            header("location:../repositorio/?controller=user&action=productos");
        }
    }
    ?>
</section>
</body>
</html>

