<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Public/Css/estilos.css">
    <link rel="icon" href="Public/Imagenes/simi.ico">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PRODUCTOS</title>
</head>
<body>
<section id='contenido' class='col-xs-12 col-sm-12 col-md-12'>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 2%">
        <div class="container-fluid">
            <a class="navbar-brand" href="" ><h2>FARMACIA</h2></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=registrar">
                            REGISTRO DE PRODUCTOS
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=editar">
                            EDICION DE PRODUCTOS
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=productos">
                            DESCRIPCION DE PRODUCTOS
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=cambioCantidad">
                            VENTA DE PRODUCTOS
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?php

    use Modelos\Productos;
    $productos = Producto::consultar();
    $fecha = Producto::fecha();
    if(isset($_GET['productos'])){
        echo"<center><h2 class='aviso'>Producto no seleccionado</h2></center>";
    }
    echo"<form  method='POST' action='../repositorio/?controller=user&action=Editar'> 
                <div class='dropdown'>
                    <select name='id' class='center' required>
                        <option value='0'>Seleccione producto</option>";
    while ($valores = mysqli_fetch_array($productos)) {
        echo "<option value=$valores[id]>$valores[id].-$valores[nombre]</option>";
    }
    echo"</select>
                </div>
                <button type='submit' class='btn btn-primary center'>Buscar</button>
            </form>";
    if(isset($_POST['id'])){
        if($_POST['id'] > 0){
            $producto = Producto::consultar($_POST['id']);
            echo"<form method='POST' action='../repositorio/?controller=user&action=editar'>
                        <div class='from-grup'>
                            <label>Id del producto</label>
                            <input type='hidden' class='form-control' name='id' value='$producto->id' >
                            <input type='text' class='form-control' value='$producto->id' disabled>
                            <label>Nombre</label>
                            <input type='text' class='form-control' name='nombre' placeholder='' value='$producto->Nombre'>
                        </div>
                        <div class='from-grup'>
                            <label>Caducidad</label>
                            <input type='number' class='form-control' name='DiaCaducidad' max='31' min='1' placeholder='Dia' value='$producto->DiaCaducidad'>
                            <input type='number' class='form-control' name='MesCaducidad' max='12' min='1' placeholder='Mes' value='$producto->MesCaducidad'>
                            <input type='number' class='form-control' name='AnioCaducidad' min='2021' max='2100' placeholder='Año' value='$producto->AnioCaducidad'>
                        </div>
                        <div class='from-grup'>
                            <label>Cantidad</label>
                            <input type='number' class='form-control' name='Cantidad' min='0' placeholder='Cantidad de producto' value='$producto->Cantidad'>
                        </div>
                        <div class='from-grup'>
                            <label>Costo</label>
                            <input type='number' class='form-control' name='Costo' min='1' placeholder='' value='$producto->Costo'>
                        </div>                         
                        <button type='submit' class='btn btn-primary'>Actualizar</button>";
            echo"</form>
                        <form method='POST' action='../repositorio/?controller=user&action=eliminar'>
                            <input type='hidden' name='id' value='$producto->id'>
                            <button type='submit' class='btn btn-primary' style='margin-top: 5px'>Eliminar</button>
                        </form>";
            if(($productos->DiaCaducidad == $fecha['dia'] && $productos->MesCaducidad == $fecha['mes'] && ((($producto->AnioCaducidad)-($fecha['anio'])) < 5)) || ($producto->MesCaducidad < $fecha['mes'] && $producto->DiaCaducidad == $fecha['dia']) || $producto->AnioCaducidad < $fecha['anio']){
                echo"<div class='from-grup'>
                            <small id='anuncio-caducidad' class='form-text aviso'>El producto caduca en menos de 5 dias.</small>
                        </div>";
            }
            if($producto->cantidad < 5){
                echo"<div class='from-grup'>
                            <small id='anuncio-existencia' class='form-text aviso'>El producto tiene menos de 5 existencias.</small>
                        </div>";
            }
        }
        else{
            header("location:../repositorio/?controller=user&action=Editat");
        }
    }
    ?>
</section>
</body>
</html>
