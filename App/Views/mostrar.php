<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>MOSTRAR</title>
</head>
<body>

<?php
if(isset($_POST['id'])){
    if($_POST['id'] > 0){
        echo"<form>
                    <input type='text' placeholder='Nombre de producto' disabled><br>
                    <input type='radio' name='sentido' value='sumar' required><label for='sumar'>sumar</label>
                    <input type='radio' name='sentido' value='restar'><label for='restar'>restar</label><br>
                    <input type='number' min='0' placeholder='Cantidad de cambio' required><br>
                    <input type='submit' value='Agregar'>
                </form>";
    }
    else{
        header("location:../repositorio/?controller=user&action=Farmacia");
    }
}
else{
    if(isset($_GET['productos'])){
        echo"<center><h2>Producto no seleccionado</h2></center>";
    }
    echo"<form method='POST' action='../repositorio/?controller=user&action=prueba'>
                <select name='id' required>
                    <option value='0'>Seleccione producto</option>
                    <option value='1'>Producto</option>
                </select><br>
                <input type='submit' value='Seleccionar'>
            </form><br><br>";
}
?>
</body>
</html>

REGISTRO DE PRODUCTO
<!DOCTYPE html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="Public/Css/estilos.css">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de productos</title>
</head>
<body>
<section id='contenido' class='col-xs-12 col-sm-12 col-md-12'>
    <nav class="navbar navbar-expand-lg navbar-light bg-light" style="margin-bottom: 20px; padding:0 2%">
        <div class="container-fluid">
            <a class="navbar-brand" href=""><h2>FARMACIA</h2></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=Registrar">
                            Registro de productos
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=Editar">
                            Edicion de productos
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=producto">
                            Descripcio de productos
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link link" href="../repositorio/?controller=user&action=vistaCambioCantidad">
                            Venta de productos
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <?php
    if(isset($_GET['productos'])){
        echo"<center><h2 class='aviso'>Producto no seleccionado</h2></center>";
    }
    ?>
    <form method='POST' action='../repositorio/?controller=user&action=registrar'>
        <div class='from-grup'>
            <label>Nombre</label>
            <input type='text' class='form-control' name='nombre' placeholder='Nombre del producto' value='' required>
        </div>
        <div class='from-grup'>
            <label>Caducidad</label>
            <input type='number' class='form-control' name='DiaCaducidad' max='31' min='1' placeholder='Dia' value='' required>
            <input type='number' class='form-control' name='MesCaducidad' max='12' min='1' placeholder='Mes' value='' required>
            <input type='number' class='form-control' name='AnioCaducidad' min='2021' max='2100' placeholder='Año' value='' required>
        </div>
        <div class='from-grup'>
            <label>Cantidad</label>
            <input type='number' class='form-control' min='0' name='Cantidad' placeholder='Cantidad inicial de producto' value='0' required>
        </div>
        <div class='from-grup'>
            <label>Costo</label>
            <input type='text' class='form-control' name='Costo' placeholder='Costo de producto' value='' required>
        </div>
        <input type='submit' class='btn btn-primary' value='Registrar'>
    </form>
</section>
</body>
</html>

