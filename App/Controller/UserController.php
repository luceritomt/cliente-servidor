<?php
include "App/Models/conexion.php";
include "App/Models/productos.php";
use Models\conexion;
use Models\productos;

class UserController
{
 //listado de productos
    public  function  productos(){
        require "App/Views/Productos.php";
    }
      //registro de los productos de farmacia
    public function registrar(){
        require"App/Views/Farmacia.php";
    }
    public function registarbd(){
        if (isset($_GET['nombre'])){
            $productos=new Productos;
            $productos->crear();
            require "App/Views/Productos.php";
        }
        else{
            header("localhost/repositorio/?controller=user&action=Database");
        }
    }
    //editar productos
    public function editar(){
        require "App/Views/Productos.php";
    }
    //editar en base de datos
    public function editarbd(){
        if (isset($_GET["id"])){
            $productos =new Productos;
            $productos->editar();
            require "App/Views/Productos.php";
        }
        else{
            header();
        }
    }
    //eliminar productos
    public function eliminar(){
        if(isset($_GET['id'])){
            $productos = new Productos;
            $productos->eliminar($_GET['id']);
            header("location:../repositorio/?controller=user&action=producto");
        }
        else{
            header("location:../repositorio/?controller=user&action=Farmacia");
        }
    }

    //compra de productps
    public function vistaCambioCantidad(){
        require "app/Views/productosExistentes.php";
    }

    //edicion de compra de productos
    public function cambioCantidad(){
        if(isset($_POST['id'])){
            $productos = new Productos;
            $productos->cambioCantidad();
            require "app/Views/Productos.php";
        }
        else{
            header("location:../repositorio/?controller=user&action=farmacia");
        }
    }
}
?>