<?php


namespace Modelos;


class productos extends conexion
{
    public $id;
    public $Nombre;
    public $DiaCaducidad;
    public $MesCaducidad;
    public $AnioCaducidad;
    public $Cantidad;
    public $Costo;

//llenado de cada campo
public function llenadocam(){
    if (isset($_GET['Nombre'])){
        $this->Nombre=$_GET['Nombre'];
    }
    if (isset($_GET['DiaCaducidad'])&& isset($_GET['MesCaducidad'])&& isset($_GET['AnioCaducidad'])){
        $this->DiaCaducidad=$_GET['DiaCaducidad'];
        $this->MesCaducidad=$_GET['MesCaducidad'];
        $this->AnioCaducidad=$_GET['AnioCaducidad'];
    }
    if (isset($_GET['Cantidad'])){
        $this->Cantidad=$_GET['Cantidad'];
    }
    if (isset($_GET['Costo'])){
        $this->Costo=$_GET['Costo'];
    }
    }
    //funcion para consultar medicamentos por nombre
    public static function consultar($Nombre){
    $conexion=new conexion;
    $pre=mysqli_prepare($conexion->connec,"SELECT *FROM farmacia WHERE Nombre=?");
    $pre->bind_param("i", $Nombre);
            $pre->execute();
            $resultado = $pre->get_result();
            return $resultado->fetch_object();
        }
        //funcion para consultar todos los medicamentos
        public static  function consultarMedi(){
    $conexion=new Conexion;
    $pre=mysqli_prepare($conexion->connec,"SELECT*FROM productos" );
    $pre->execute();
    $re=$pre->get_result();
    return $re;
    }
    //funcion para crear productos
    public function crear(){
        $this->llenadocam();
        $pre = mysqli_prepare($this->connec, "INSERT INTO productos(Nombre,DiaCaducidad,MesCaducidad,AnioCaducidad,Cantidad,Costo) VALUES (?, ?, ?, ?, ?, ?)");
        $pre->bind_param("ssssss", $this->Nombre, $this->DiaCaducidad, $this->MesCaducidad, $this->AnioCaducidad, $this->Cantidad, $this->Costo);
        $pre->execute();
    }
    //funcion para  editar productos
    public function editar(){
        $this->llenadocam();
        $pre = mysqli_prepare($this->connec, "UPDATE productos SET Nombre = ?,DiaCaducidad= ?, MesCaducidad= ?,AnioCaducidad= ?,Cantidad= ?,Costo= ? WHERE id = ?");
        $pre->bind_param("sssssss", $this->Nombre, $this->DiaCaducidad, $this->MesCaducidad, $this->AnioCaducidad, $this->Cantidad, $this->Costo, $this->id);
        $pre->execute();
    }
//funcion para eliminar productos de la base de datos
    public function eliminar($id){
        $pre = mysqli_prepare($this->connec, "DELETE FROM productos WHERE id = ?");
        $pre->bind_param("s", $id);
        $pre->execute();
    }
    //funcion para la fecha de los productos
    public static function fechap(){
        $fechap = getdate();
        return $fechap;
    }
    //funcion para cambiar la cantidad de productos
    public function Cantidad(){
        $this->llenadocam();
        $pre = '';
        if($_GET['sentido'] == "suma"){
            $pre = mysqli_prepare($this->connec, "UPDATE productos SET Cantidad = Cantidad + ? WHERE id = ?");
        }
        elseif($_POST['sentido'] == "resta") {
            $pre = mysqli_prepare($this->connec, "UPDATE productos SET Cantidad = Cantidad - ? WHERE id = ?");
        }
        $pre->bind_param("ss", $this->Cantidad, $this->id);
        $pre->execute();
    }

}
?>